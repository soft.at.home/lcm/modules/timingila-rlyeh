/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxm/amxm.h>
#include <amxd/amxd_common.h>
#include <amxb/amxb.h>

#include <timingila/timingila_defines.h>
#include <rlyeh/rlyeh_defines.h>
#include <timingila/timingila_signals.h>

#include <timingila/timingila_module.h>

#include "timingila_rlyeh_priv.h"
#include "timingila_common.h"

#define ME "rlyeh"

#define TIMINGILA_RLYEH_METHOD "timingilarlyehmethod"
#define TIMINGILA_RLYEH_METHOD_UNKNOWN 0x00
#define TIMINGILA_RLYEH_METHOD_CREATEDU 0x01
#define TIMINGILA_RLYEH_METHOD_UPDATEDU 0x02
#define TIMINGILA_RLYEH_METHOD_UNINSTALLDU 0x03
#define TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT 0x04
#define EVENT_RLYEH_TRIGGER_GC "pkg:rlyeh:gc"

// DM mngr defines
#define AMXM_SELF "self"

#define UNKNOWN "Unknown"
#define WHEN_STR_EMPTY_RETURN_UNKNOWN(str) (((str) && (str)[0] != '\0') ? (str) : UNKNOWN)

#define PARAMS "parameters"

#define SIZE_ALIAS 64

// shared object bookkeeping
static amxm_shared_object_t * so = NULL;
static amxm_module_t* mod = NULL;

// bus context for Rlyeh
static amxb_bus_ctx_t* amxb_bus_ctx_rlyeh = NULL;

// track if we are subscribed to rlyeh events
static bool rlyeh_subscribed = false;

// command tracker
static timingila_command_llist_type rlyeh_command_llist;
static timingila_command_id_type_t rlyeh_command_id_counter = 0;

static void rlyeh_notify_handler(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv);

static void wait_and_connect_rlyeh(void);
static int remove_slot(const char* const signal_name, amxp_slot_fn_t fn);

static inline amxb_bus_ctx_t* rlyeh_get_amxb_bus_ctx(void) {
    return amxb_bus_ctx_rlyeh;
}

static int rlyeh_emit_signal(const char* signal_name, const amxc_var_t* const data) {
    int retval = -1;
    SAH_TRACEZ_INFO(ME, "Emitting signal: %s", signal_name);
    retval = timingila_emit_signal(signal_name, data);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldn't emit signal: %s [%d]", signal_name, retval);
    }
    return retval;
}

// Generic callback to be used to clean up the structures
static void rlyeh_default_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                  amxb_request_t* req,
                                  int status,
                                  void* priv) {
    SAH_TRACEZ_INFO(ME, RLYEH " default request is done - status = %d", status);
    amxb_close_request(&req);
    if(priv) {
        amxc_var_t* args = (amxc_var_t*) priv;
        amxc_var_delete(&args);
    }
    return;
}

static void rlyeh_images_list_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                      amxb_request_t* req,
                                      int status,
                                      void* priv) {
    rlyeh_default_done_cb(NULL, req, status, priv);
    if(status == amxd_status_ok) {
        rlyeh_emit_signal(EVENT_TIMINGILA_LATE_INIT_DONE, NULL);
    }
}


static void rlyeh_pull_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                               amxb_request_t* req,
                               int status,
                               void* priv) {
    SAH_TRACEZ_INFO(ME, RLYEH " pull request is done - status = %d", status);
    if(status != amxd_status_ok) {
        // TODO: maybe move to rlyeh:error handler
        /*
           uint64_t timingila_cmdid = GET_UINT64((amxc_var_t*) priv, AMXB_DEFERRED_CALLID);
           amxc_var_t* data = amxd_function_deferred_get_priv(timingila_cmdid);
           amxc_var_t retvar;
           axmc_var_init(&retvar);
           amxc_var_set_type(&retvar, AMXC_VAR_ID_HTABLE);
           amxc_var_add_key(uint32_t, &retvar, "err_code", );
         */
        //amxd_function_deferred_done(timingila_cmdid, amxd_status_unknown_error, NULL, <>);
        rlyeh_emit_signal(EVENT_PACKAGER_INSTALL_DU_FAILED, (const amxc_var_t*) priv);
        //amxc_var_clean(&retvar);
        //free(data);
    }
    rlyeh_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void rlyeh_gc_done_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                             UNUSED amxb_request_t* req,
                             int status,
                             UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, RLYEH " gc request is done - status = %d", status);
    rlyeh_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static void rlyeh_rm_cb(UNUSED const amxb_bus_ctx_t* bus_ctx,
                        amxb_request_t* req,
                        int status,
                        void* priv) {
    SAH_TRACEZ_INFO(ME, RLYEH " remove request is done - status = %d", status);
    if(status != amxd_status_ok) {
        // TODO: maybe move to rlyeh:error handler
        rlyeh_emit_signal(EVENT_PACKAGER_UNINSTALL_DU_FAILED, (const amxc_var_t*) priv);
    }
    rlyeh_default_done_cb(bus_ctx, req, status, NULL);
    return;
}

static int event_dustatechange_from_rlyeh(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        goto exit;
    }

    amxm_ret = timingila_dm_event_execute(TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_event_execute(" TIMINGILA_MOD_DM_EVENT_DUSTATECHANGE ") return code %d",
                    amxm_ret);

    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

static int update_du_from_rlyeh(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        goto exit;
    }

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_DU ") return code %d",
                    amxm_ret);

    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

static int remove_du_from_rlyeh(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    int amxm_ret = 0;

    amxc_var_init(&ret);

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        goto exit;
    }

    amxm_ret = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_REMOVE_DU, data, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_REMOVE_DU ") return code %d",
                    amxm_ret);

exit:
    amxc_var_clean(&ret);
    return retval;
}

static int update_du_from_rlyeh_parsed(amxc_var_t* data) {
    int retval = -1;
    const char* duid = NULL;
    const char* version = NULL;
    const char* url = NULL;
    const char* vendor = NULL;
    const char* description = NULL;
    const char* name = NULL;
    amxc_var_t* args = NULL;

    if(data == NULL) {
        goto exit;
    }

    duid = GET_CHAR(data, RLYEH_DM_IMAGE_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " RLYEH_DM_IMAGE_DUID);
        goto exit;
    }

    version = GET_CHAR(data, RLYEH_DM_IMAGE_VERSION);
    url = GET_CHAR(data, RLYEH_DM_IMAGE_URI);
    vendor = GET_CHAR(data, RLYEH_DM_IMAGE_VENDOR);
    description = GET_CHAR(data, RLYEH_DM_IMAGE_DESCRIPTION);
    name = GET_CHAR(data, RLYEH_DM_IMAGE_NAME);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_URL, url);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VENDOR, vendor);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DESCRIPTION, description);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_NAME, name);

    retval = update_du_from_rlyeh(args);
    amxc_var_delete(&args);
exit:
    return retval;
}

static int remove_du_from_rlyeh_parsed(amxc_var_t* notification_parameters) {
    int retval = 1;
    const char* duid = NULL;
    const char* version = NULL;
    amxc_var_t* args = NULL;

    if(notification_parameters == NULL) {
        SAH_TRACEZ_ERROR(ME, "No notification data given");
        goto exit;
    }

    duid = GET_CHAR(notification_parameters, RLYEH_DM_IMAGE_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " RLYEH_DM_IMAGE_DUID);
        goto exit;
    }
    version = GET_CHAR(notification_parameters, RLYEH_DM_IMAGE_VERSION);
    if(version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " RLYEH_DM_IMAGE_VERSION);
        goto exit;
    }

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, args, SM_DM_DU_VERSION, version);

    retval = remove_du_from_rlyeh(args);
    amxc_var_delete(&args);
exit:
    return retval;
}

static int rlyeh_command(const char* command, amxc_var_t* args, amxb_be_done_cb_fn_t cb, void* priv) {
    int retval = -2;
    amxb_request_t* request = NULL;
    char command_id_str[SIZE_COMMAND_ID_STR];
    unsigned int command_id = 0;
    amxc_var_t* rlyeh_command_args = NULL;

    if(cb == NULL) {
        SAH_TRACEZ_ERROR(ME, "Callback is NULL");
        goto exit;
    }

    command_id = rlyeh_command_id_counter++;

    if(timingila_command_id_to_hex_str(command_id_str, command_id) < 1) {
        SAH_TRACEZ_ERROR(ME, "command_id_str might be malformed");
    }

    amxc_var_new(&rlyeh_command_args);
    amxc_var_set_type(rlyeh_command_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_command_args, RLYEH_COMMAND, command);
    amxc_var_add_key(cstring_t, rlyeh_command_args, RLYEH_NOTIF_COMMAND_ID, command_id_str);
    amxc_var_add_key(amxc_htable_t, rlyeh_command_args, RLYEH_COMMAND_PARAMETERS, amxc_var_constcast(amxc_htable_t, args));

    SAH_TRACEZ_INFO(ME, RLYEH RLYEH_COMMAND ": %s", command);
    if(timingila_command_add_new(&rlyeh_command_llist, command_id, priv)) {
        SAH_TRACEZ_ERROR(ME, "Couldn't add command");
    }

    request = amxb_async_call(rlyeh_get_amxb_bus_ctx(),
                              RLYEH,
                              RLYEH_COMMAND,
                              rlyeh_command_args,
                              cb,
                              priv);
    if(request == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot do " RLYEH ".%s()", command);
        goto exit;
    }

    retval = 0;
exit:
    amxc_var_delete(&rlyeh_command_args);
    return retval;
}

static int rlyeh_remove(const amxc_var_t* const args,
                        uint32_t method) {
    int rc = -1;
    const char* duid = NULL;
    const char* version = NULL;
    amxc_var_t* priv_args = NULL;
    amxc_var_t* rlyeh_args = NULL;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        goto exit;
    }

    // decode args
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_DUID);
        goto exit;
    }

    version = GET_CHAR(args, SM_DM_DU_VERSION);
    if(version == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing " SM_DM_DU_VERSION);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Attempting to " RLYEH_CMD_REMOVE ": %s:%s", duid, version);

    // translate args to rlyeh
    amxc_var_new(&rlyeh_args);
    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_DM_IMAGE_DUID, duid);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_DM_IMAGE_VERSION, version);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_RLYEH_METHOD, method);

    rc = rlyeh_command(RLYEH_CMD_REMOVE, rlyeh_args, rlyeh_rm_cb, (void*) priv_args);
    amxc_var_delete(&rlyeh_args);
exit:
    return rc;
}

static int rlyeh_pull(const amxc_var_t* args) {
    int rc = -1;
    const char* url = NULL;
    const char* username = NULL;
    const char* password = NULL;
    const char* duid = NULL;
    const char* signature = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_t* priv_args = NULL;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        goto exit;
    }

    // decode args
    url = GET_CHAR(args, SM_DM_DU_URL);
    username = GET_CHAR(args, SM_DM_DU_USERNAME);
    password = GET_CHAR(args, SM_DM_DU_PASSWORD);
    duid = GET_CHAR(args, SM_DM_DU_DUID);
    signature = GET_CHAR(args, SM_DM_DU_SIGNATURE);

    // translate args to rlyeh
    amxc_var_new(&rlyeh_args);
    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_URI, url);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_DUID, duid);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_USERNAME, username);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_PASSWORD, password);
    if(signature) {
        SAH_TRACEZ_WARNING(ME, "Overloading signature: '%s'", signature);
        amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_SIGNATURE_URL, signature);
    }

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_RLYEH_METHOD, TIMINGILA_RLYEH_METHOD_CREATEDU);

    rc = rlyeh_command(RLYEH_CMD_PULL, rlyeh_args, rlyeh_pull_done_cb, (void*) priv_args);
    amxc_var_delete(&rlyeh_args);
exit:
    return rc;
}

static int rlyeh_update(const amxc_var_t* args) {
    int rc = -1;
    const char* url = NULL;
    const char* username = NULL;
    const char* password = NULL;
    const char* duid = NULL;
    amxc_var_t* rlyeh_args = NULL;
    amxc_var_t* priv_args = NULL;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        goto exit;
    }

    // decode args
    url = GET_CHAR(args, SM_DM_DU_URL);
    username = GET_CHAR(args, SM_DM_DU_USERNAME);
    password = GET_CHAR(args, SM_DM_DU_PASSWORD);

    duid = GET_CHAR(args, SM_DM_DU_DUID);

    // translate args to rlyeh
    amxc_var_new(&rlyeh_args);
    amxc_var_set_type(rlyeh_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_URI, url);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_DUID, duid);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_USERNAME, username);
    amxc_var_add_key(cstring_t, rlyeh_args, RLYEH_CMD_PULL_PASSWORD, password);

    // create priv
    amxc_var_new(&priv_args);
    amxc_var_copy(priv_args, args);
    amxc_var_add_key(uint32_t, priv_args, TIMINGILA_RLYEH_METHOD, TIMINGILA_RLYEH_METHOD_UPDATEDU);

    rc = rlyeh_command(RLYEH_CMD_PULL, rlyeh_args, rlyeh_pull_done_cb, (void*) priv_args);
    amxc_var_delete(&rlyeh_args);
exit:
    return rc;
}


// TODO: replace hardcoded keys
static int add_du_from_rlyeh(amxc_var_t* data) {
    int retval = -1;
    amxc_var_t ret;
    const char* executionenvref = NULL;
    const char* duid = NULL;
    const char* uuid = NULL;
    const char* version = NULL;
    const char* uri = NULL;
    const char* vendor = NULL;
    const char* name = NULL;
    const char* description = NULL;
    const char* status = NULL;
    char alias[SIZE_ALIAS] = {'\0'};
    amxc_var_t* du_args = NULL;

    amxc_var_init(&ret);

    if(data == NULL) {
        SAH_TRACEZ_ERROR(ME, "No data given");
        goto exit;
    }

    executionenvref = GET_CHAR(data, SM_DM_DU_EE_REF);
    duid = GET_CHAR(data, RLYEH_DM_IMAGE_DUID);
    version = GET_CHAR(data, RLYEH_DM_IMAGE_VERSION);
    uri = GET_CHAR(data, RLYEH_DM_IMAGE_URI);
    vendor = GET_CHAR(data, RLYEH_DM_IMAGE_VENDOR);
    name = GET_CHAR(data, RLYEH_DM_IMAGE_NAME);
    description = GET_CHAR(data, RLYEH_DM_IMAGE_DESCRIPTION);
    // add status
    status = GET_CHAR(data, SM_DM_DU_STATUS);
    uuid = GET_CHAR(data, SM_DM_DU_UUID);

    if(snprintf(alias, SIZE_ALIAS, "cpe-%s", duid) < 1) {
        SAH_TRACEZ_ERROR(ME, SM_DM_DU_ALIAS " might be malformed");
    }

    amxc_var_new(&du_args);
    amxc_var_set_type(du_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_DUID, duid);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_URL, uri);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_EE_REF, WHEN_STR_EMPTY_RETURN_UNKNOWN(executionenvref));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_VERSION, WHEN_STR_EMPTY_RETURN_UNKNOWN(version));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_ALIAS, alias);
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_NAME, WHEN_STR_EMPTY_RETURN_UNKNOWN(name));
    if(status == NULL) {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
    } else {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_STATUS, status);
    }
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_VENDOR, WHEN_STR_EMPTY_RETURN_UNKNOWN(vendor));
    amxc_var_add_key(cstring_t, du_args, SM_DM_DU_DESCRIPTION, WHEN_STR_EMPTY_RETURN_UNKNOWN(description));
    if(uuid) {
        amxc_var_add_key(cstring_t, du_args, SM_DM_DU_UUID, uuid);
    }

    retval = timingila_dm_mngr_execute(TIMINGILA_MOD_DM_MNGR_UPDATE_DU, du_args, &ret);
    SAH_TRACEZ_INFO(ME,
                    "timingila_dm_mngr_execute(" TIMINGILA_MOD_DM_MNGR_UPDATE_DU ") return code %d",
                    retval);
    amxc_var_delete(&du_args);

exit:
    amxc_var_clean(&ret);
    return retval;
}

static int add_slot(const char* const signal_name,
                    const char* const expression,
                    amxp_slot_fn_t fn,
                    void* const priv) {
    int retval = -255;
    if((signal_name == NULL) || (signal_name[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        goto exit;
    }

    if(fn == NULL) {
        SAH_TRACEZ_ERROR(ME, "function is NULL");
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Adding slot: %s [%p]",
                    signal_name,
                    fn);

    retval = timingila_add_slot_to_signal(signal_name, expression, fn, priv);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Error adding slot [%d]", retval);
    }
exit:
    return retval;
}

static int update_amxb_bus_ctx_rlyeh(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Update amxb bus ctx for: " RLYEH_DM);

    amxb_bus_ctx_rlyeh = amxb_be_who_has(RLYEH_DM);
    if(!amxb_bus_ctx_rlyeh) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " RLYEH_DM);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int subscribe_to_rlyeh(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Subscribe to: " RLYEH_DM);

    retval = amxb_subscribe(amxb_bus_ctx_rlyeh,
                            RLYEH_DM,
                            NULL,
                            rlyeh_notify_handler,
                            NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot subscribe to " RLYEH_DM " [%d]", retval);
    }

    return retval;
}

static void slot_wait_connect_rlyeh(UNUSED const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    wait_and_connect_rlyeh();
}

static void slot_rlyeh_up(const char* const sig_name,
                          UNUSED const amxc_var_t* const data,
                          UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(rlyeh_subscribed) {
        return;
    }
    if(update_amxb_bus_ctx_rlyeh() < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not initialize Rlyeh bus context");
        goto exit;
    }
    if(subscribe_to_rlyeh()) {
        SAH_TRACEZ_ERROR(ME, "Cannot subscribe to rlyeh yet");
        goto exit;
    }

    remove_slot(AMXB_WAIT_DONE, slot_rlyeh_up);

    rlyeh_subscribed = true;
    return;
exit:
    amxp_sigmngr_deferred_call(NULL, slot_wait_connect_rlyeh, NULL, NULL);
    return;
}

static void wait_and_connect_rlyeh(void) {
    SAH_TRACEZ_INFO(ME, "Waiting on " RLYEH_DM ".");
    amxb_wait_for_object(RLYEH_DM ".");
    add_slot(AMXB_WAIT_DONE, NULL, slot_rlyeh_up, NULL);
}

static void slot_exec_pkg_uninstall_du(const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    rlyeh_remove(data, TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT);
}

static void slot_container_uninstall_du_done(const char* const sig_name,
                                             const amxc_var_t* const data,
                                             UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    rlyeh_remove(data, TIMINGILA_RLYEH_METHOD_UNINSTALLDU);
}

static void slot_container_validate_install_du_done(const char* const sig_name,
                                                    const amxc_var_t* const data,
                                                    UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    rlyeh_pull(data);
}

static void slot_container_validate_update_du_done(const char* const sig_name,
                                                   const amxc_var_t* const data,
                                                   UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    rlyeh_update(data);
}

#ifndef RLYEH_CMD_LIST
#define RLYEH_CMD_LIST "list"
#endif

static void slot_rlyeh_trigger_gc(const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  UNUSED void* priv) {
    amxc_var_t* newpriv;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);

    amxc_var_new(&newpriv);
    amxc_var_set_type(newpriv, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, newpriv, TIMINGILA_RLYEH_METHOD, TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT);

    rlyeh_command(RLYEH_CMD_GC, NULL, rlyeh_gc_done_cb, newpriv);
}

static void slot_packager_uninstall_du_done(const char* const sig_name,
                                            UNUSED const amxc_var_t* const data,
                                            UNUSED void* priv) {
    amxc_var_t* newpriv;
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);

    amxc_var_new(&newpriv);
    amxc_var_set_type(newpriv, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, newpriv, TIMINGILA_RLYEH_METHOD, TIMINGILA_RLYEH_METHOD_UNINSTALLDU);

    rlyeh_command(RLYEH_CMD_GC, NULL, rlyeh_gc_done_cb, newpriv);
}

static void slot_timingila_late_init_start(const char* const sig_name,
                                           UNUSED const amxc_var_t* const data,
                                           UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "Signal '%s' received", sig_name);
    rlyeh_command(RLYEH_CMD_LIST, NULL, rlyeh_images_list_done_cb, NULL);
}

static int remove_slot(const char* const signal_name,
                       amxp_slot_fn_t fn) {
    int retval = -255;
    SAH_TRACEZ_INFO(ME, "Removing slot: %s [%p]", signal_name, fn);
    if((signal_name == NULL) || (signal_name[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "signal_name is empty");
        goto exit;
    }

    if(fn == NULL) {
        SAH_TRACEZ_ERROR(ME, "function is NULL");
        goto exit;
    }

    retval = timingila_remove_slot_from_signal(signal_name, fn);
exit:
    return retval;
}

static int rlyeh_rm_uninstall(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    int rc = -1;
    bool blenable = false;

    if(args == NULL) {
        SAH_TRACEZ_ERROR(ME, "No args");
        goto exit;
    }

    blenable = GET_BOOL(args, "enable");
    if(blenable) {
        SAH_TRACEZ_WARNING(ME, "Enable rmafteruninstall");
        add_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, NULL, slot_packager_uninstall_du_done, NULL);
    } else {
        SAH_TRACEZ_WARNING(ME, "Disable rmafteruninstall");
        remove_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, slot_packager_uninstall_du_done);
    }

exit:
    return rc;
}

static amxc_var_t* amxc_var_add_or_update_key_cstring_t(amxc_var_t* const var,
                                                        const char* key,
                                                        const char* const val) {
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_get_key(var, key, AMXC_VAR_FLAG_DEFAULT);
    if(!subvar) {
        subvar = amxc_var_add_new_key(var, key);
    }
    when_null(subvar, exit);

    if(amxc_var_set_cstring_t(subvar, val) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;
}

static void rlyeh_notify_handler(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    timingila_command_entry_t* cmd = NULL;
    SAH_TRACEZ_INFO(ME, AMXM_NOTIFICATION " received [%s]:", sig_name);

    const char* command_id = GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID);
    const char* notification = GET_CHAR(data, AMXM_NOTIFICATION);
//    const char* path = GET_CHAR(data, "path");

    if(notification == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot find notification field");
        return;
    }

    if(command_id && (command_id[0] != '\0')) {
        cmd = timingila_command_find(&rlyeh_command_llist, timingila_command_id_from_hex_str(command_id));
    }

    if(strcmp(notification, RLYEH_NOTIF_ERROR) == 0) {
        // TODO: add proper logging
        SAH_TRACEZ_INFO(ME, RLYEH_NOTIF_ERROR);
        if(cmd) {
            // Command found so procedure started by us
            // --> cannot use parameters will be NULL
            // TODO: differentiate between updatedu and installdu ???
            uint64_t callid = GET_UINT64(cmd->priv, AMXB_DEFERRED_CALLID);
            const char* uuid = GET_CHAR(cmd->priv, SM_DM_DU_UUID);
            uint32_t faulttype = GET_UINT32(data, RLYEH_NOTIF_ERROR_TYPE);
            const char* faulttypestr = GET_CHAR(data, RLYEH_NOTIF_ERROR_TYPESTR);
            const char* reason = GET_CHAR(data, RLYEH_NOTIF_ERROR_REASON);
            if(!string_is_empty(reason)) {
                faulttypestr = reason;
            }
            amxc_var_t* notif_data = NULL;

            // cleanup deferred call
            {
                amxc_var_t deferred_data;
                amxc_var_init(&deferred_data);
                amxc_var_set_type(&deferred_data, AMXC_VAR_ID_HTABLE);
                amxc_var_add_key(uint32_t, &deferred_data, AMXB_DEFERRED_ERR_CODE, faulttype);
                amxc_var_add_key(cstring_t, &deferred_data, AMXB_DEFERRED_ERR_MSG, faulttypestr);

                amxd_function_deferred_done(callid, amxd_status_unknown_error, &deferred_data, NULL);

                amxc_var_clean(&deferred_data);
            }

            amxc_var_new(&notif_data);
            amxc_var_set_type(notif_data, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_UUID, uuid);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_DU_STATUS, SM_DM_EVENT_DUSTATECHANGE_FAILED);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_INSTALL);
            amxc_var_add_key(uint32_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTCODE, faulttype);
            amxc_var_add_key(cstring_t, notif_data, SM_DM_EVENT_DUSTATECHANGE_FAULTSTRING, faulttypestr);
            event_dustatechange_from_rlyeh(notif_data);
            amxc_var_delete(&notif_data);
        }
    } else if(strcmp(notification, RLYEH_NOTIF_IMAGE_PULLED) == 0) {
        SAH_TRACEZ_INFO(ME, RLYEH_NOTIF_IMAGE_PULLED);
        amxc_var_t* parameters = GET_ARG(data, PARAMS);
        if(cmd) {
            const char* name = GET_CHAR(parameters, RLYEH_DM_IMAGE_NAME);
            const char* version = GET_CHAR(parameters, RLYEH_DM_IMAGE_VERSION);
            const char* disklocation = GET_CHAR(parameters, RLYEH_DM_IMAGE_DISKLOCATION);
            const char* vendor = GET_CHAR(parameters, RLYEH_DM_IMAGE_VENDOR);
            const char* description = GET_CHAR(parameters, RLYEH_DM_IMAGE_DESCRIPTION);
            const char* duid = GET_CHAR(parameters, RLYEH_DM_IMAGE_DUID);
            const char* oldversion = GET_CHAR(cmd->priv, SM_DM_DU_VERSION);
            const char* uuid = GET_CHAR(cmd->priv, SM_DM_DU_UUID);
            const char* executionenvref = GET_CHAR(cmd->priv, "ExecutionEnvRef");
            uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_RLYEH_METHOD);

            if(duid) {
                SAH_TRACEZ_INFO(ME, "We have CMD for DUID: '%s'", duid);
            }

            if(executionenvref) {
                amxc_var_add_key(cstring_t, parameters, "ExecutionEnvRef", executionenvref);
            }

            switch(method) {
            case TIMINGILA_RLYEH_METHOD_CREATEDU:
                amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
                amxc_var_add_key(cstring_t, parameters, SM_DM_DU_UUID, uuid);
                add_du_from_rlyeh(parameters);
                break;
            case TIMINGILA_RLYEH_METHOD_UPDATEDU:
                if(oldversion && (oldversion[0] != '\0')) {
                    amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_VERSION_OLD, oldversion);
                } else {
                    SAH_TRACEZ_ERROR(ME, "Missing oldversion = %p", oldversion);
                }
                amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UPDATING);
                update_du_from_rlyeh_parsed(parameters);
                break;
            default:
                SAH_TRACEZ_ERROR(ME, "Cannot determine method (%u)", method);
                break;
            }

            // Add name
            if(name && version && vendor && description) {
                amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_NAME, name);
                amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_VERSION, version);
                amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_VENDOR, vendor);
                amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_DESCRIPTION, description);
                if(!string_is_empty(disklocation)) {
                    amxc_var_add_or_update_key_cstring_t(cmd->priv, SM_DM_DU_DISKLOCATION, disklocation);
                }
                switch(method) {
                case TIMINGILA_RLYEH_METHOD_CREATEDU:
                    rlyeh_emit_signal(EVENT_PACKAGER_INSTALL_DU_DONE, cmd->priv);
                    break;
                case TIMINGILA_RLYEH_METHOD_UPDATEDU:
                    // update version
                    rlyeh_emit_signal(EVENT_PACKAGER_UPDATE_DU_DONE, cmd->priv);
                    break;
                default:
                    SAH_TRACEZ_ERROR(ME, "Cannot determine method (%u)", method);
                }
            }
        } else {
            // for events not issued by commands of this module
            // not sure if it will work though
            amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_INSTALLING);
            add_du_from_rlyeh(parameters);

        }
    } else if(strcmp(notification, RLYEH_NOTIF_IMAGE_MARKED_RM) == 0) {
        SAH_TRACEZ_INFO(ME, RLYEH_NOTIF_IMAGE_MARKED_RM);
        if(cmd) {
            uint32_t method = GET_UINT32(cmd->priv, TIMINGILA_RLYEH_METHOD);
            switch(method) {
            case TIMINGILA_RLYEH_METHOD_UNINSTALLDU:
                SAH_TRACEZ_INFO(ME, "TIMINGILA_RLYEH_METHOD_UNINSTALLDU");
                if(GET_ARG(cmd->priv, AMXB_DEFERRED_CALLID) != NULL) {
                    uint64_t callid = GET_UINT64(cmd->priv, AMXB_DEFERRED_CALLID);
                    amxd_function_deferred_done(callid, amxd_status_ok, NULL, NULL);
                } else {
                    SAH_TRACEZ_WARNING(ME, "Cannot find " AMXB_DEFERRED_CALLID);
                }
                rlyeh_emit_signal(EVENT_PACKAGER_UNINSTALL_DU_DONE, cmd->priv);
                break;
            case TIMINGILA_RLYEH_METHOD_UPDATEDU:
                SAH_TRACEZ_INFO(ME, "TIMINGILA_RLYEH_METHOD_UPDATEDU --> silent gc");
                rlyeh_emit_signal(EVENT_RLYEH_TRIGGER_GC, NULL);
                goto exit;
            case TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT:
                SAH_TRACEZ_INFO(ME, "TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT --> silent gc");
                rlyeh_emit_signal(EVENT_RLYEH_TRIGGER_GC, NULL);
                goto exit;
            default:
                SAH_TRACEZ_WARNING(ME, "Couldn't match method (%u)", method);
                rlyeh_emit_signal(EVENT_PACKAGER_UNINSTALL_DU_DONE, cmd->priv);
                break;
            }
        }
        amxc_var_t* parameters = GET_ARG(data, PARAMS);
        amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UNINSTALLED);
        update_du_from_rlyeh_parsed(parameters);
    } else if(strcmp(notification, RLYEH_NOTIF_IMAGE_REMOVED) == 0) {
        SAH_TRACEZ_INFO(ME, RLYEH_NOTIF_IMAGE_REMOVED);
        amxc_var_t* parameters = GET_ARG(data, PARAMS);
        if(cmd) {
            uint32_t method = TIMINGILA_RLYEH_METHOD_UNKNOWN;
            if(cmd->priv) {
                method = GET_UINT32(cmd->priv, TIMINGILA_RLYEH_METHOD);
            }
            switch(method) {
            case TIMINGILA_RLYEH_METHOD_UNINSTALLDU:
                amxc_var_add_key(cstring_t, parameters, SM_DM_DU_STATUS, SM_DM_DU_STATUS_UNINSTALLED);
                amxc_var_add_key(cstring_t, parameters, SM_DM_EVENT_DUSTATECHANGE_OPERATION, SM_DM_EVENT_DUSTATECHANGE_OPERATION_UNINSTALL);
                event_dustatechange_from_rlyeh(parameters);
                break;
            case TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT:
                SAH_TRACEZ_INFO(ME, "TIMINGILA_RLYEH_METHOD_UNINSTALLDU_SILENT --> no dustatechange");
                break;
            case TIMINGILA_RLYEH_METHOD_UNKNOWN:
                SAH_TRACEZ_WARNING(ME, "TIMINGILA_RLYEH_METHOD_UNKNOWN");
                break;
            default:
                SAH_TRACEZ_WARNING(ME, "Cannot determine method");
                break;
            }
        } else {
            SAH_TRACEZ_WARNING(ME, "No cmd");
        }
        remove_du_from_rlyeh_parsed(parameters);
    } else if(strcmp(notification, RLYEH_NOTIF_IMAGE_LIST) == 0) {
        SAH_TRACEZ_INFO(ME, RLYEH_NOTIF_IMAGE_LIST);
        amxc_var_t* images = GET_ARG(data, RLYEH_NOTIF_IMAGES);
        if(images) {
            amxc_var_for_each(image, images) {
                bool markforremove = GET_BOOL(image, RLYEH_DM_IMAGE_MARK_RM);
                if(!markforremove) {
                    update_du_from_rlyeh_parsed(image);
                }
            }
        }
    } else if(strcmp(notification, AMXB_APP_START) == 0) {
        SAH_TRACEZ_WARNING(ME, RLYEH " is started again, the module will work again");
    } else if(strcmp(notification, AMXB_APP_STOP) == 0) {
        SAH_TRACEZ_ERROR(ME, RLYEH " is stopped, the module will not work properly");
        rlyeh_subscribed = false;
        wait_and_connect_rlyeh();
    }

exit:
    if(cmd) {
        SAH_TRACEZ_ERROR(ME, "removing command: %s", command_id);
        timingila_command_remove(&cmd);
    }

    return;
}

static void init_command_id_counter(void) {
    SAH_TRACEZ_INFO(ME, "Intialize command id counter with random");
    rlyeh_command_id_counter = timingila_init_command_id_counter();
    SAH_TRACEZ_INFO(ME, "rlyeh_command_id_counter = %X", rlyeh_command_id_counter);
    return;
}

static void init_rlyeh_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Initialize rlyeh_command_llist");
    if(timingila_command_llist_init(&rlyeh_command_llist)) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize rlyeh_command_llist");
        return;
    }
}

static void init_slots(void) {
    int retval;

    SAH_TRACEZ_ERROR(ME, "Initializing internal signals");
    retval = timingila_create_signal(EVENT_RLYEH_TRIGGER_GC);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt create signal " EVENT_RLYEH_TRIGGER_GC);
    }

    SAH_TRACEZ_INFO(ME, "Initializing slots");
    if(retval == 0) {
        retval = add_slot(EVENT_RLYEH_TRIGGER_GC, NULL, slot_rlyeh_trigger_gc, NULL);
        if(retval) {
            SAH_TRACEZ_ERROR(ME, "Couldnt slot " EVENT_RLYEH_TRIGGER_GC);
        }
    }
    retval = add_slot(EVENT_CONTAINER_UNINSTALL_DU_DONE, NULL, slot_container_uninstall_du_done, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot " EVENT_CONTAINER_UNINSTALL_DU_DONE);
    }
    retval = add_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, NULL, slot_packager_uninstall_du_done, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot " EVENT_PACKAGER_UNINSTALL_DU_DONE);
    }
    retval = add_slot(EVENT_TIMINGILA_LATE_INIT_HANDOVER, NULL, slot_timingila_late_init_start, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot " EVENT_TIMINGILA_LATE_INIT_HANDOVER);
    }
    retval = add_slot(EVENT_EXEC_PACKAGER_UNINSTALL_DU, NULL, slot_exec_pkg_uninstall_du, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot" EVENT_EXEC_PACKAGER_UNINSTALL_DU);
    }
    retval = add_slot(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE, NULL, slot_container_validate_install_du_done, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot" EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE);
    }
    retval = add_slot(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE, NULL, slot_container_validate_update_du_done, NULL);
    if(retval) {
        SAH_TRACEZ_ERROR(ME, "Couldnt slot" EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE);
    }

    return;
}

static void remove_slots(void) {
    SAH_TRACEZ_INFO(ME, "Removing slots");
    remove_slot(EVENT_CONTAINER_UNINSTALL_DU_DONE, slot_container_uninstall_du_done);
    remove_slot(EVENT_PACKAGER_UNINSTALL_DU_DONE, slot_packager_uninstall_du_done);
    remove_slot(EVENT_TIMINGILA_LATE_INIT_HANDOVER, slot_timingila_late_init_start);
    remove_slot(EVENT_EXEC_PACKAGER_UNINSTALL_DU, slot_exec_pkg_uninstall_du);
    remove_slot(AMXB_WAIT_DONE, slot_rlyeh_up);
    remove_slot(EVENT_RLYEH_TRIGGER_GC, slot_rlyeh_trigger_gc);
    remove_slot(EVENT_CONTAINER_VALIDATE_INSTALL_DU_DONE, slot_container_validate_install_du_done);
    remove_slot(EVENT_CONTAINER_VALIDATE_UPDATE_DU_DONE, slot_container_validate_update_du_done);
    return;
}

static void cleanup_rlyeh_command_llist(void) {
    SAH_TRACEZ_INFO(ME, "Cleanup rlyeh_command_llist");
    timingila_command_llist_cleanup(&rlyeh_command_llist);
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-rlyeh constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(ME, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, MOD_TIMINGILA_PACKAGER)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", MOD_TIMINGILA_PACKAGER);
        goto exit;
    }

    init_rlyeh_command_llist();
    init_command_id_counter();
    update_amxb_bus_ctx_rlyeh();
    amxm_module_add_function(mod, TIMINGILA_PACKAGER_RMAFTERUNINSTALL, rlyeh_rm_uninstall);
    init_slots();
    wait_and_connect_rlyeh();

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< timingila-rlyeh constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> timingila-rlyeh destructor\n");
    amxb_unsubscribe(amxb_bus_ctx_rlyeh, RLYEH_DM, rlyeh_notify_handler, NULL);
    remove_slots();
    cleanup_rlyeh_command_llist();
    SAH_TRACEZ_INFO(ME, "<<< timingila-rlyeh destructor\n");
    return 0;
}
