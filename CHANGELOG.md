# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.1.0 - 2024-10-02(09:14:09 +0000)

### Other

- Cthulhu API validation

## Release v1.0.8 - 2024-07-17(21:31:40 +0000)

### Other

- Create USP Controller instance when a container added

## Release v1.0.7 - 2024-07-16(13:15:19 +0000)

### Other

- DUStateChange! event missing on DU Uninstallation

## Release v1.0.6 - 2024-05-30(13:49:11 +0000)

### Other

- fix memory leaks

## Release v1.0.5 - 2023-12-08(09:33:24 +0000)

### Other

- Initialize return variables

## Release v1.0.4 - 2023-11-06(12:22:14 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v1.0.3 - 2023-10-04(13:04:36 +0000)

### Other

- Opensource component

## Release v1.0.2 - 2023-09-07(12:20:23 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v1.0.1 - 2023-06-23(11:40:41 +0000)

### Other

- Move signature from Annotations to installDU() parameter

## Release v1.0.0 - 2023-06-02(13:45:18 +0000)

### Other

- DU/EU Unique Identifier

## Release v0.2.14 - 2023-03-14(20:16:06 +0000)

### Other

- [Cthulhu] upgrading container when not enough space on disk...

## Release v0.2.13 - 2023-02-27(10:14:19 +0000)

### Other

- [timingila] Use STAGING_LIBDIR instead of LIBDIR in src/makefile

## Release v0.2.12 - 2023-02-24(14:37:06 +0000)

### Other

- DU installation failure should be reported as such
- [softwaremodule] "Version is empty" fail is popping when updating DU

## Release v0.2.11 - 2023-02-07(13:55:27 +0000)

### Other

- [Timingila-rlyeh] Missing information on reboot

## Release v0.2.10 - 2023-01-18(12:52:22 +0000)

### Other

- DU instance created and not destroyed when DU installation fails

## Release v0.2.9 - 2022-11-03(10:05:06 +0000)

### Other

- Put the usp error in the args variant instead of ret

## Release v0.2.8 - 2022-10-25(07:50:17 +0000)

### Other

- allow for several subdirs on repo

## Release v0.2.7 - 2022-09-08(12:07:26 +0000)

### Other

- Unexpected removed DU after an Update

## Release v0.2.6 - 2022-06-23(15:53:23 +0000)

### Fixes

- - wait for Rlyeh to be on bus before subscribing on events

## Release v0.2.5 - 2022-03-04(09:54:16 +0000)

## Release v0.2.4 - 2021-12-15(03:59:05 +0000)

## Release v0.2.3 - 2021-12-13(17:36:35 +0000)

## Release v0.2.2 - 2021-12-03(03:06:59 +0000)

### Other

- Implement DeploymentUnit Update

## Release v0.2.1 - 2021-11-23(21:23:45 +0000)

## Release v0.2.0 - 2021-10-07(11:17:33 +0000)

### New

- [Timingila] Implement DU.Uninstall()

## Release v0.1.2 - 2021-09-14(15:07:07 +0000)

## Release v0.1.1 - 2021-09-14(11:59:00 +0000)

## Release v0.1.0 - 2021-09-10(09:11:27 +0000)

